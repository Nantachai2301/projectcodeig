<?php include('headerlogin.php')?> 
<center><h2>เข้าสู่ระบบ</h2><hr><br></center>
<center>  
<div class="container md-6 ">
<div class="container">
        <div class="row justify-content-md-center">
            <div class="col-5">

                <?php if(session()->getFlashdata('msg')):?>
                    <div class="alert alert-warning">
                       <?= session()->getFlashdata('msg') ?>
                    </div>
                <?php endif;?>
                <form action="<?php echo base_url(); ?>/SigninController/loginAuth" method="post">
                    <div class="form-group mb-3">
                        <input type="idcard" name="idcard" placeholder="เลขบัตรประชาชน 13 หลัก" value="<?= set_value('idcard') ?>" class="form-control" >
                    </div>
                    <div class="form-group mb-3">
                        <input type="password" name="password" placeholder="Password" class="form-control" >
                    </div>
                    
                    <div class="d-grid">
                         <button type="submit" class="btn btn-success">Signin</button>
                    </div>     
                </form>
            </div>
              
        </div>
    </div>
 
</form>
     
    
 </div>
</div>
</div>
</center>
<?php include ('footer.php')?> 
