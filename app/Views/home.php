<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSS only -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

    <title>Document</title>

<body>
<style>
body { 

  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.header {
  overflow: hidden;
  background-color: #800000;
  padding:10px 10px;
 
}
.red-box {
  		background:#000	;
          padding:8px 118px;
  	}
.d-box {
  		background:#000	;
          padding:8px 118px;
  	}
.button{
  padding: 8px;
}
h4{
  color:#FFF;
}
hr{
  background-color: #FFF;
}
.card-link {
     color: red;               
     text-decoration:none;   
  }
  .card-link-end {
     color:#FFF;              
     text-decoration:none;    
  }
</style>
</head>
<body>
<div class="header">
   <img src="https://www.npru.ac.th/2019/img/logo.png "alt="alternatetext"  ></div>
   <div class="red-box"></div>
</div>
<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="true">
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
  </div>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="https://www.npru.ac.th/admin/file_images/20221005104213_3f910ca9790287a35640e8f6d0eb62c0.png" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="https://scontent.fbkk12-5.fna.fbcdn.net/v/t39.30808-6/310147256_535684908559686_8180386566879434267_n.jpg?_nc_cat=107&ccb=1-7&_nc_sid=e3f864&_nc_eui2=AeFOP-5sMaGQ3-CLCMEDdt9cLB7Ely6Sj5ksHsSXLpKPmVEcIW4TvALE_QyIG3PaNKK6tOO0f0sqBuIHgt2LTQcj&_nc_ohc=2I7HL9uxa-QAX9NS951&_nc_zt=23&_nc_ht=scontent.fbkk12-5.fna&oh=00_AfAGwjNUmoOV5ndlM31mrlbXWlZbgeA9uBNkD1rLpfU6jA&oe=63639A76" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="https://www.npru.ac.th/2019/img/slide/banner1920x600-03.jpg" class="d-block w-100" alt="...">
    </div>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>

</div><br>
<h4>* กิจกรรมหน่วยงาน</h4>
<hr  size=  "10px"> 
<div class="row row-cols-15 row-cols-md-7 g-5 28rem">
<div class="col-sm">
    <div class="card">
      <img src="https://news.npru.ac.th/userfiles/PR/nm_images/20221027145150_%E0%B8%81%E0%B8%9A%2011.jpg" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">คณะมนุษยศาสตร์และสังคมศาสตร์ ลงนา...</h5>
      <a href="#" class="card-link">รายละเอียดเพิ่มเติม</a>
      </div>
      </div>
  </div>
  <div class="col">
    <div class="card">
      <img src="https://news.npru.ac.th/userfiles/PR/nm_images/20221005123115_%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%8A%E0%B8%B8%E0%B8%A1%E0%B8%A7%E0%B8%B4%E0%B8%8A%E0%B8%B2%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%99%E0%B8%B2%E0%B8%99%E0%B8%B2%E0%B8%8A%E0%B8%B2%E0%B8%95%E0%B8%B4%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%AA%E0%B8%AD%E0%B8%99%E0%B8%A0%E0%B8%B2%E0%B8%A9%E0%B8%B2%E0%B8%88%E0%B8%B5%E0%B8%99.jpg" class="card-img-top" alt="...">
      <div class="card-body">
       <h5 class="card-title">โครงการประชุมวิชาการนานาชาติทางด้...</h5>
       <a href="#" class="card-link">รายละเอียดเพิ่มเติม</a>
      </div>
      </div>
  </div>
  <div class="col">
    <div class="card">
      <img src="https://news.npru.ac.th/userfiles/PR/nm_images/20220805094822_%E0%B8%81%E0%B8%9A%208.jpg" class="card-img-top" alt="...">
      <div class="card-body">
      <h5 class="card-title">การประชุมคณะกรรมการบริหารมหาวิทยา...</h5>
       <a href="#" class="card-link">รายละเอียดเพิ่มเติม</a>
      </div>
     </div>
     </div>
  <div class="col">
    <div class="card">
      <img src="https://news.npru.ac.th/userfiles/PR/nm_images/20220803121407_%E0%B8%82%E0%B8%B1%E0%B8%9A%E0%B8%82%E0%B8%B5%E0%B9%88%E0%B8%9B%E0%B8%A5%E0%B8%AD%E0%B8%94%E0%B8%A0%E0%B8%B1%E0%B8%A2.jpg" class="card-img-top" alt="...">
      <div class="card-body">
      <h5 class="card-title">กองพัฒนานักศึกษา จัดโครงการขับขี่.</h5>
       <a href="#" class="card-link">รายละเอียดเพิ่มเติม</a>
      </div>
     </div><br>
     <h6 class ="text-end">
     <a href="#" class="card-link-end">ข่าวทั้งหมด</a>  </div>
     <div class = container>
