<?php include('headerregister.php')?>   
<div class="container mt-4">
        <div class="row justify-content-md-center">
        <div class="col-6">
                <h2>ลงทะเบียนผู้สมัคร</h2><hr>
                <?php if(isset($validation)):?>
                <div class="alert alert-danger">
                   <?= $validation->listErrors() ?>
                </div>
                <?php endif;?>
                <form action="<?php echo base_url(); ?>/SignupController/store" method="post">
                    <div class="form-group mb-3">
                    <label for="inputname" class="form-label">ชื่อ-นามสกุล</label>
                        <input type="text" name="name" placeholder="Name" value="<?= set_value('name') ?>" class="form-control" >
                    </div>
                    <div class="form-group mb-3">
                        <label for="idcard" class="form-label">เลขบัตรประชาชน</label>
                        <input type="text" name="idcard"placeholder="เลขบัตรประชาชน 13 หลัก" class="form-control"  value="<?= set_value('idcard'); ?>" class="form-control" >
                    </div>
                    <div class="form-group mb-3">
                    <label for="inputemail" class="form-label">Email</label>
                        <input type="email" name="email" placeholder="Email" value="<?= set_value('email') ?>" class="form-control" >
                    </div>
                    <div class="form-group mb-3">
                    <label for="inputpassword" class="form-label">รหัสผ่าน</label>
                        <input type="password" name="password" placeholder="Password" class="form-control" >
                    </div>
                    <div class="form-group mb-3">
                    <label for="inputpasswordd" class="form-label">ยืนยันรหัสผ่าน</label>
                        <input type="password" name="confirmpassword" placeholder="Confirm Password" class="form-control" >
                    </div>
                    
                 <center><button type="submit" class="btn btn-success">ลงทะเบียน</button>
                </form>
                <hr>
            </div>
        </div>
    </div>


<?php include ('footer.php')?> 
