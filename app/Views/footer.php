<style>
.footer {
    background: #B03060;
    padding-top: 30px;
}

.footer .footer-logo .navbar-brand {
    color: #fff;
    font-family: 'Prompt', sans-serif;
    font-size: 18px;
    font-weight: 500;
    letter-spacing: 0.1em;
}

.footer .footer-logo p {
    color: #fff;
    font-size: 12px;
    letter-spacing: 1px;
}

.footer .footer-logo {
    color: #fff;
    font-size: 12px;
    line-height: 25px;
    letter-spacing: 1px;
    font-weight: 200;
}

.footer .list-menu>h4 {
    color: #fff;
    padding-bottom: 3px;
}

.footer .list-menu ul li a {
    color: #fff;
    font-size: 12px;
    font-weight: 200;

}

.footer .copyrights {
    background: #000000;
    color: #fff;
    text-align: left;
    padding: 8px;
}

.footer .copyrights p,
.footer .copyrights .credits {
    margin: 0;
    padding: 0;
    font-family: 'Prompt', sans-serif;
    font-weight: normal;
    font-size: 14px;
    letter-spacing: 0;
    color: #fff;
}

.footer .copyrights p a,
.footer .copyrights .credits a {
    letter-spacing: 0;
    color: #fff;
}

.footer .social-links {
    padding-top: 10px;
}

.footer .social-links a {
    font-size: 18px;
    display: inline-block;

    color: #fff;
    line-height: 1;

    text-align: center;
    width: 36px;
    height: 36px;
    transition: 0.3s;
}


.owl-carousel owl-theme .item {
    margin: 3px;
}

.owl-carousel owl-theme .item img {
    display: block;
    width: 65%;
    height: auto;
}
</style>
<footer class="footer">
            <div class="container">
                <div class="row">
                    <link href="2019/lib/fontawesome-free-6.1.1-web/css/all.css" rel="stylesheet">
                    <div class="col-sm-6 col-md-6 col-lg-5">
                        <div class="footer-logo">

                            <div class="col-md-12" style="font-size:18px;font-weight: 500;letter-spacing: 0.1em;">
                                มหาวิทยาลัยราชภัฏนครปฐม</div>
                            <div class="col-md-12">Nakhon Pathom Rajabhat University </div>
                            <div class="col-md-12"><i class="fas fa-map-marker-alt"></i> 85 ถนนมาลัยแมน อ.เมือง จ.นครปฐม
                                73000
                            </div>
                            <div class="col-md-12"><i class="fas fa-fax"></i> 0 3426 1048</div>
                            <div class="col-md-12"><i class="fas fa-phone"></i> 0 3410 9300</div>
                            <div class="col-md-12"><a href="mailto: saraban@npru.ac.th" target="_blank"
                                    style="color: #FFF; text-decoration:none;"><i class="fas fa-envelope"></i>
                                    saraban@npru.ac.th</a></div>
                            <div class="col-md-12"><a href="https://vt.tiktok.com/ZSR8UasYw/" target="_blank"
                                    style="color: #FFF; text-decoration:none;"><i class="fa-brands fa-tiktok"></i>
                                    pr_npru </a>
                            </div>
                            <div class="col-md-12">
                                <a href="https://www.facebook.com/NPRUPRUNIT" class="facebook" target="_blank"
                                    style="color: #FFF; text-decoration:none;"><i class="fab fa-facebook"></i>
                                    ประชาสัมพันธ์
                                    ม.ราชภัฏนครปฐม PR NPRU</a><br><br>
                                <!-- <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
            <a href="#" class="instagram"><i class="fab fa-instagram"></i></a> -->

                            </div>
                        </div>
                        <div class="col-md-12">
                            <iframe iframe="" width="100%" height="150"
                                src="https://maps.google.com/maps?width=100%&amp;height=450&amp;hl=en&amp;q=%E0%B8%A1%E0%B8%AB%E0%B8%B2%E0%B8%A7%E0%B8%B4%E0%B8%97%E0%B8%A2%E0%B8%B2%E0%B8%A5%E0%B8%B1%E0%B8%A2%E0%B8%A3%E0%B8%B2%E0%B8%8A%E0%B8%A0%E0%B8%B1%E0%B8%8F%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B8%9B%E0%B8%90%E0%B8%A1+(Title)&amp;ie=UTF8&amp;t=&amp;z=15&amp;iwloc=B&amp;output=embed"
                                frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                        </div>

                        <p>
                        </p>
                    </div>





                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="list-menu">
                            <div class="col-md-12">
                                <h4 style="color:#FFF;">ระบบสารสนเทศ</h4>

                                <ul class="list-unstyled">
                                    <li><a href="http://reg2.npru.ac.th/registrar/apphome.asp" target="_blank">&gt;&gt;
                                            ระบบรับสมัครนักศึกษา</a></li>
                                    <li><a href="app1.php" target="_blank">&gt;&gt; ระบบสารสนเทศสำหรับนักศึกษา</a></li>
                                    <li><a href="app2.php" target="_blank">&gt;&gt; ระบบสารสนเทศสำหรับบุคลากร</a></li>
                                    <li><a href="app3.php" target="_blank">&gt;&gt; ระบบสารสนเทศสำหรับบุคคลทั่วไป</a>
                                    </li>
                                </ul>

                                <h4 style="color:#FFF;">เว็บไซต์ภายในมหาวิทยาลัย</h4>

                                <ul class="list-unstyled">
                                    <li><a href="http://www.npru.ac.th/npru_sitemap.php" target="_blank">&gt;&gt;
                                            ลิงค์เว็บไซต์หน่วยงานทั้งหมด</a></li>
                                    <li><a href="http://dept.npru.ac.th/cp/" target="_blank">&gt;&gt;
                                            เว็บไซต์อาจารย์</a></li>
                                    <li><a href="http://dept.npru.ac.th/cp/index.php?act=6a992d5529f459a44fee58c733255e86&amp;lntype=extmod&amp;sys=sys_article&amp;dat=index&amp;mac_id=2496"
                                            target="_blank">&gt;&gt; เว็บไซต์สาขาวิชา</a></li>
                                    <li><a href="http://phone.npru.ac.th/" target="_blank">&gt;&gt;
                                            เบอร์โทรศัพท์หน่วยงานภายใน</a></li>
                                </ul>

                            </div>
                            <div class="col-md-12">
                                <!--  <script>
  (function() {
    var cx = '018387909514839141753:fztttpjzksc';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>  >-->
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-2">
                        <div class="list-menu">

                            <!-- IPv6-test.com button BEGIN -->
                            <div style="margin-top:10px; margin-bottom:10px;"><a
                                    href="http://ipv6-test.com/validate.php?url=www.npru.ac.th"><img
                                        src="https://www.npru.ac.th/2019/img/icon/button-ipv6-small.png"
                                        alt="ipv6 ready" title="ipv6 ready" border="0"></a></div>
                            <!-- IPv6-test.com button END -->

                            <div id="ipv6_enabled_www_test_logo"></div>
                            <script language="JavaScript" type="text/javascript">
                                var Ipv6_Js_Server = "https://";
                                document.write(unescape("%3Cscript src='" + Ipv6_Js_Server +
                                    "www.ipv6forum.com/ipv6_enabled/sa/SA1.php?id=5676' type='text/javascript'%3E%3C/script%3E"
                                ));
                            </script>
                            <script src="https://www.ipv6forum.com/ipv6_enabled/sa/SA1.php?id=5676"
                                type="text/javascript">
                            </script>
                            <a href="http://ipv6-test.com"><img src="http://v4v6.ipv6-test.com/imgtest.png"
                                    alt="ipv6 test" title="ipv6 test" border="0"></a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="copyrights">
                <div class="container">
                    <p>Copyrights © 2019 Nakhon Pathom Rajabhat University. All rights reserved</p>
                    <div class="credits">
                        <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=eStartup
          
          Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>-->
                    </div>
                </div>
            </div>

        </footer>
    </center>
    <script src="https://code.jquery.com/jquery-3.6.1.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
</body>

</html>